from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
db_name = 'db2.sqlite'

# Database
app.config['SECRTE_KEY'] = 'mysecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, db_name)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Init Marshmallow
marsh = Marshmallow(app)


# Product Class
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True)
    description = db.Column(db.String(200))
    price = db.Column(db.Float)
    qty = db.Column(db.Integer)

    def __init__(self, name, description, price, qty):
        self.name = name
        self.description = description
        self.price = price
        self.qty = qty


# Product Schema
class ProductSchema(marsh.Schema):
    class Meta:
        fields = ('id', 'name', 'description', 'price', 'qty')


# Init Schema
product_schema = ProductSchema()
products_schema = ProductSchema()

# Create a product
@app.route('/products', methods=['POST'])
def add_product():
    name = request.json['name']
    description = request.json['description']
    price = request.json['price']
    qty = request.json['qty']

    new_product = Product(name, description, price, qty)
    db.session.add(new_product)
    db.session.commit()

    return product_schema.jsonify(new_product)

# Get all products
@app.route('/products', methods=['GET'])
def get_products():
    products = Product.query.all()
    output = []

    for product in products:
        prod_data = dict()
        prod_data['id'] = product.id
        prod_data['name'] = product.name
        prod_data['description'] = product.description
        prod_data['price'] = product.price
        prod_data['qty'] = product.qty
        output.append(prod_data)

    return jsonify({'products': output})

# Get single product
@app.route('/product/<prod_id>', methods=['GET'])
def get_product(prod_id):
    product = Product.query.get(prod_id)
    return product_schema.jsonify(product)

# Update product
@app.route('/product/<prod_id>', methods=['PUT'])
def update_product(prod_id):
    product = Product.query.get(prod_id)

    product.name = request.json['name']
    product.description = request.json['description']
    product.price = request.json['price']
    product.qty = request.json['qty']

    db.session.commit()
    return product_schema.jsonify(product)

# Delete product
@app.route('/product/<prod_id>', methods=['DELETE'])
def delete_product(prod_id):
    product = Product.query.get(prod_id)
    db.session.delete(product)
    db.session.commit()
    return product_schema.jsonify(product)

@app.route('/', methods=['POST', 'GET'])
def main_page():
    return jsonify({"message": "This is a your main page. Please check '/product' end point."})


# Run server
if __name__ == '__main__':
    app.run(debug=True)



